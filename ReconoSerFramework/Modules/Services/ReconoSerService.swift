//
//  ReconoSerService.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

protocol ReconoSerService {
    
    func uploadS3(imageBase64:String, defaultFolder:String, completionHandler: @escaping (NSDictionary?, ErrorEntransaccion?) -> ())
    
    func compareFaces(sourceImage:String, targetImage:String, completionHandler: @escaping (CompararRostro?, ErrorEntransaccion?) -> ())
    
    func consultarConvenio(guidConv: String, datosConv: String, completionHandler: @escaping (ConsultarConvenio?, ErrorEntransaccion?) -> ())
    
    func enviarOTP(guidCiu:String, datosOTP:DatosOTP, completionHandler: @escaping (EnviarOTP?, ErrorEntransaccion?) -> ())
    
    func validarOTP(guidOTP:String, datosOTP:String, completionHandler: @escaping (ValidarOTP?, ErrorEntransaccion?) -> ())
    
    func guardarCiudadano(ciudadano:CiudadanoIn, completionHandler: @escaping (GuardarCiudadano?, ErrorEntransaccion?) -> ())
    
    func consultarCiudadano(guidCiu:String, completionHandler: @escaping (ConsultarCiudadano?, ErrorEntransaccion?) -> ())
    
    func guardarBiometria(validarBiometria:ValidarBiometriaIn, completionHandler: @escaping (GuardarBiometria?, ErrorEntransaccion?) -> ())
    
    func validarBiometria(validarBiometria:ValidarBiometriaIn, completionHandler: @escaping (ValidarBiometria?, ErrorEntransaccion?) -> ())
    
    func guardarlogError(error:GuardarLogErrorIn, completionHandler: @escaping (GuardarLogError?, ErrorEntransaccion?) -> ())
    
    func solicitarPreguntasDemograficas(guidCiu:String, completionHandler: @escaping (SolicitarPreguntaDemografica?, ErrorEntransaccion?) -> ())
    
    func validarRespuestaDemograficas(guidCiu: String, idCuestionario:String, registroCuestionario:Int, respuestas:[RespuestasIn], completionHandler: @escaping (ValidarRespuestaDemografica?, ErrorEntransaccion?) -> ())
    
    func guardarDocumento(tipoDocumento: String, barcode: String, completionHandler: @escaping (GuardarDocumento?, ErrorEntransaccion?) -> ())
}

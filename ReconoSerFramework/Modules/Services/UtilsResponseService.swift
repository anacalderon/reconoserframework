//
//  UtilsRequestService.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

struct Service {
    var id: Int
    var name: String
    var type: String
    var subType: String
    var retries: Int
    var waitRetries: Int
}

struct Agreement {
    let state: Bool
    let service: [Service]
}

private let _sharedInstance = UtilsResponseService()

internal class UtilsResponseService {
    
    var dataConv:Agreement?
    
    class var sharedInstance: UtilsResponseService {
        return _sharedInstance
    }
    
    func saveAgreement(data:ConsultarConvenio) {
        var services:[Service] = []
        for _service in data.serviciosConv {
            
            let service = Service(id: _service.idServicio,
                                  name: _service.nombreServicio,
                                  type: _service.tipoServicio,
                                  subType: _service.subtipo,
                                  retries: _service.reintentos,
                                  waitRetries: _service.esperaReintentos)
            services.append(service)
        }
        dataConv = Agreement(state: true, service: services)
    }
    
    func parseGetAgreementResponse(data: NSDictionary) -> ConsultarConvenio? {
        
        let _state = data["EstadoActivo"]
        let _services = data["ServiciosConv"]
        let services = _services as? [NSDictionary]
        
        var listServices:[ServiciosConv] = []
        for _data in (services ?? []) {
            let step = _data["ContinuarProceso"]
            let retries = _data["Reintentos"]
            let waitRetries = _data["EsperaReintentos"]
            let id = _data["IdServicio"]
            let captureLevel = _data["NivelCaptura"]
            let name = _data["NombreServicio"]
            let subtype = _data["SubTipo"]
            let type = _data["TipoServicio"]
            
            let service = ServiciosConv(idServicio:id as? Int ?? 0,
                                        nombreServicio: name as? String ?? "",
                                        tipoServicio: type as? String ?? "" ,
                                        subtipo: subtype as? String ?? "",
                                        nivelCaptura: captureLevel as? Int ?? 0,
                                        continuarProceso: (step as? Int ?? 0) != 0,
                                        reintentos: retries as? Int ?? 0,
                                        esperaReintentos: waitRetries as? Int ?? 0)
            listServices.append(service)
        }
        
        let answerTransaction = parseAnswerTransaction(data: data)
        let getAgreementData = ConsultarConvenio(estadoActivo: (_state as? Int ?? 0) != 0, serviciosConv: listServices, respuestaTransaccion: answerTransaction)
        
        return getAgreementData
    }
    
    func parseSendOTP(data: NSDictionary) -> EnviarOTP? {
        
        let idOTP = data["GuidOTP"]
        let answerTransaction = parseAnswerTransaction(data: data)
        
        let sendOTPData = EnviarOTP(guidOTP: idOTP as? String ?? "", respuestaTransaccion: answerTransaction)
        return sendOTPData
    }
    
    func parseValidateOTP(data: NSDictionary) -> ValidarOTP? {
        
        let isValid = data["EsValida"]
        let answerTransaction = parseAnswerTransaction(data: data)
        
        let validateOTPData = ValidarOTP(esValida: (isValid as? Int ?? 0) != 0, respuestaTransaccion: answerTransaction)
        return validateOTPData
    }
    
    func parseSaveResident(data: NSDictionary) -> GuardarCiudadano? {
        
        let guidCiu = data["GuidCiu"]
        let firstName = data["PrimerNombre"]
        let secondName = data["SegundoNombre"]
        let lastname = data["PrimerApellido"]
        let secondLastname = data["SegundoApellido"]
        let alive = data["Vivo"]
        let status = data["EstadoDoc"]
        let expirationDate = data["FechaExp"]
        let birthdate = data["FechaNac"]
        let location = data["Localizacion"]
        
        let answerTransaction = parseAnswerTransaction(data: data)
        
        let saveResidentData = GuardarCiudadano(guidCiu: guidCiu as? String ?? "",
                                                primerNombre: firstName as? String ?? "",
                                                segundoNombre: secondName as? String ?? "",
                                                primerApellido: lastname as? String ?? "",
                                                segundoApellido: secondLastname as? String ?? "",
                                                vivo: (alive as? Int ?? 0) != 0,
                                                estadoDoc: status as? String ?? "",
                                                fechaExp: expirationDate as? String ?? "",
                                                fechaNac: birthdate as? String ?? "",
                                                localizacion: location as? String ?? "",
                                                respuestaTransaccion: answerTransaction)
        return saveResidentData
    }
    
    func parseConsultResident(data: NSDictionary) -> ConsultarCiudadano? {
        
        let resident = data["Ciudadano"] as? NSDictionary
        let _status = data["Status"]
        
        let id = resident?["GuidCiu"]
        let conv = resident?["GuidConv"]
        let type = resident?["TipoDoc"]
        let num = resident?["NumDoc"]
        let email = resident?["Email"]
        let codeCountry = resident?["CodPais"]
        let phone = resident?["Celular"]
        let additionalData = resident?["DatosAdi"]
        
        let residentData = Ciudadano(guidCiu: id as? String ?? "",
                                     guidConv: conv as? String ?? "",
                                     tipoDoc: type as? String ?? "",
                                     numDoc: num as? String ?? "",
                                     email: email as? String ?? "",
                                     codPais: codeCountry as? String ?? "",
                                     celular: phone as? String ?? "",
                                     datosAdi: additionalData as? String ?? "")
        
        let status = _status as? [NSDictionary]
        
        var listStatus:[Status] = []
        for _data in (status ?? []) {
            let service = _data["Servicio"]
            let finished = _data["Terminado"]
            
            let dataStatus = Status(servicio: service as? String ?? "",
                                    terminado: (finished as? Int ?? 0) != 0)
            listStatus.append(dataStatus)
        }
        
        let answerTransaction = parseAnswerTransaction(data: data)
        let consultResidentData = ConsultarCiudadano(ciudadano: residentData, status: listStatus, respuestaTransaccion: answerTransaction)
        return consultResidentData
    }
    
    func parseSaveBiometry(data: NSDictionary) -> GuardarBiometria? {
        
        let guid = data["GuidBio"]
        let answerTransaction = parseAnswerTransaction(data: data)
        
        let saveBiometryData = GuardarBiometria(guidBio: guid as? String ?? "",
                                                respuestaTransaccion: answerTransaction)
        return saveBiometryData
    }
    
    func parseValidateBiometry(data: NSDictionary) -> ValidarBiometria? {
        
        let isValid = data["EsValido"]
        let score = data["Score"]
        let answerTransaction = parseAnswerTransaction(data: data)
        
        let validateBiometryData = ValidarBiometria(esValido: (isValid as? Int ?? 0) != 0,
                                                    score: score as? String ?? "",
                                                    respuestaTransaccion: answerTransaction)
        return validateBiometryData
    }
    
    func parseSaveLogError(data: NSDictionary) -> GuardarLogError? {
        
        let _isSuccess = data["EsExitosa"]
        let _error = data["ErrorEntransaccion"]
        
        var dataError:ErrorEntransaccion? = nil
        if (_error != nil) {
            let errors = _error as? [NSDictionary]
            for errorData in (errors ?? []) {
                let code = errorData["Codigo"]
                let description = errorData["Descripcion"]
                dataError = ErrorEntransaccion(codigo: code as? String ?? "",
                                               descripcion:description as? String ?? "")
            }
        }
        
        let saveLogErrorData = GuardarLogError(esExitosa: (_isSuccess as? Int ?? 0) != 0, errorEntransaccion:dataError)
        
        return saveLogErrorData
    }
    
    func parseRequestQuestions(data: NSDictionary) -> SolicitarPreguntaDemografica? {
        let id = data["IdCuestionario"]
        let registerQuestionnaire = data["RegistroCuestionario"]
        let _question = data["Preguntas"]
        let question = _question as? [NSDictionary]
        
        var listQuestion:[Pregunta] = []
        for _data in (question ?? []) {
            let id = _data["IdPregunta"]
            let text = _data["TextoPregunta"]
            let options = _data["OpcionesRespuestas"] as? [NSDictionary]
            
            var listAnswers:[OpcionRespuesta] = []
            for _option in (options ?? []) {
                let idAnswer = _option["IdRespuesta"]
                let textAnswer = _option["TextoRespuesta"]
                
                let answer = OpcionRespuesta(idRespuesta: idAnswer as? String ?? "",
                                             textoRespuesta: textAnswer as? String ?? "")
                listAnswers.append(answer)
            }
            
            let dataQuestion = Pregunta(idPregunta: id as? String ?? "",
                                        textoPregunta: text as? String ?? "",
                                        opcionesRespuestas: listAnswers)
            listQuestion.append(dataQuestion)
        }
        
        let answerTransaction = parseAnswerTransaction(data: data)
        let requestQuestionsData = SolicitarPreguntaDemografica(idCuestionario: id as? String ?? "", registroCuestionario: registerQuestionnaire as? Int ?? 0, Preguntas: listQuestion, respuestaTransaccion: answerTransaction)
        return requestQuestionsData
    }
    
    func parseValidateAnswers(data: NSDictionary) -> ValidarRespuestaDemografica? {
        let isValid = data["EsValido"]
        let score = data["Score"]
        let answerTransaction = parseAnswerTransaction(data: data)
        
        let validateAnswersData = ValidarRespuestaDemografica(score: score as? String ?? "",
                                                              esValido: (isValid as? Int ?? 0) != 0,
                                                              respuestaTransaccion: answerTransaction)
        return validateAnswersData
    }
    
    func parseAnswerTransaction(data: NSDictionary) -> RespuestaTransaccion {
        
        let answerTransaction = data["RespuestaTransaccion"] as? NSDictionary
        let _isSuccess = answerTransaction?["EsExitosa"]
        let _error = answerTransaction?["ErrorEntransaccion"]
        
        var dataError:ErrorEntransaccion? = nil
        if (_error != nil) {
            let errors = _error as? [NSDictionary]
            for errorData in (errors ?? []) {
                let code = errorData["Codigo"]
                let description = errorData["Descripcion"]
                dataError = ErrorEntransaccion(codigo: code as? String ?? "",
                                               descripcion:description as? String ?? "")
            }
        }
        
        let dataResponse = RespuestaTransaccion(esExitosa: (_isSuccess as? Int ?? 0) != 0, errorEntransaccion:dataError)
        
        return dataResponse
    }
    
    func parseSaveDocument(data: NSDictionary) -> GuardarDocumento? {
        let numDoc = data["NumeroDocumento"]
        let lastname = data["PrimerApellido"]
        let secondLastname = data["SegundoApellido"]
        let firstName = data["PrimerNombre"]
        let secondName = data["SegundoNombre"]
        let sex = data["Sexo"]
        let date = data["FechaNacimiento"]
        let rh = data["RH"]
        
        let answerTransaction = parseAnswerTransaction(data: data)
        
        let saveDocumentData = GuardarDocumento(numDoc: numDoc as? String ?? "",
                                                primerApellido: lastname as? String ?? "",
                                                segundoApellido: secondLastname as? String ?? "",
                                                primerNombre: firstName as? String ?? "",
                                                segundoNombre: secondName as? String ?? "",
                                                sexo: sex as? String ?? "",
                                                fechaDeNacimiento: date as? String ?? "",
                                                rh: rh as? String ?? "",
                                                respuestaTransaccion: answerTransaction)
        
        return saveDocumentData
    }
    
    func parseCompareFaces(data: NSDictionary) -> CompararRostro {
        let _data = data["data"] as? NSDictionary
        let faceMatches = _data?["FaceMatches"] as? [NSDictionary]
        
        var similarity:Double = 0.0
        if ((faceMatches?.count ?? 0) > 0) {
            similarity = faceMatches?[0]["Similarity"] as? Double ?? 0.0
        }
        
        return CompararRostro(Similitud: similarity)
    }
    
    func validarConvenio(nameService:String, typeService:String, subtypeService:String, completionHandler: @escaping (Bool, ErrorEntransaccion?) -> ()) {
        
        print("Validate Agreement ---> ", nameService, typeService, subtypeService)
        
        if (self.dataConv == nil) {
            let error = ReconoSerConstants.Error.ERROR_SERVER + ": Por favor consulte el convenio"
            completionHandler(false, ErrorEntransaccion(codigo: ReconoSerConstants.Error.ERROR_R103, descripcion: error))
            return
        }
        
        if (!self.dataConv!.state) {
            completionHandler(false, ErrorEntransaccion(codigo: ReconoSerConstants.Error.ERROR_R101, descripcion:  ReconoSerConstants.Error.ERROR_CONV))
            return
        }
        
        var isValid = false
        for _service in self.dataConv?.service ?? [] {
            if (_service.name == nameService) {
                
                if ((_service.type.range(of: typeService) != nil) && (_service.subType.range(of: subtypeService) != nil)) {
                    isValid = true
                    break
                }
                
                if ((_service.type.range(of: typeService) != nil) && (_service.subType.isEmpty && subtypeService.isEmpty)) {
                    isValid = true
                    break
                }
            }
        }
        
        if (!isValid) {
            completionHandler(false, ErrorEntransaccion(codigo: ReconoSerConstants.Error.ERROR_R102, descripcion:  ReconoSerConstants.Error.ERROR_SERVICE_API))
        } else {
            completionHandler(true, nil)
        }
    }
}

//
//  RequestService.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation
import Alamofire

private let _sharedInstance = ServiciosOlimpia()

public class ServiciosOlimpia : NSObject, ReconoSerService {
    
    @objc public override init() {
        super.init()
    }
    
    public class var sharedInstance: ServiciosOlimpia {
        return _sharedInstance
    }
    
    public func consultarConvenio(guidConv: String, datosConv: String, completionHandler: @escaping (ConsultarConvenio?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["guidConv"] = guidConv
        data["Datos"] = datosConv
        
        UserDefaults.standard.set("guidConv", forKey: guidConv)
        
        let path = "ConsultarConvenio"
        _olimpiaPost(path: path, parameters: data, completionHandler: {(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let consultarConvenio = UtilsResponseService.sharedInstance.parseGetAgreementResponse(data: data!)!
            UtilsResponseService.sharedInstance.saveAgreement(data: consultarConvenio)
            
            if (!consultarConvenio.respuestaTransaccion.esExitosa) {
                completionHandler(nil, consultarConvenio.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(consultarConvenio, error)
        })
    }
    
    public func enviarOTP(guidCiu: String, datosOTP: DatosOTP, completionHandler: @escaping (EnviarOTP?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        var otp:Dictionary<String, Any> = Dictionary()
        otp["TipoOTP"] = datosOTP.tipoOTP
        otp["Mensaje"] = datosOTP.mensaje
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: otp,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            data["DatosOTP"] = theJSONText
        }
        data["GuidCiudadano"] = guidCiu
        
        let path = "EnviarOTP"
        UtilsResponseService.sharedInstance.validarConvenio(nameService: ReconoSerConstants.Name.OTP.nameService, typeService: datosOTP.tipoOTP, subtypeService: ReconoSerConstants.Name.OTP.SubTypeService, completionHandler: { (isSuccess, error) in
            if (isSuccess) {
                self._olimpiaPost(path: path, parameters: data, completionHandler: {(data, error) in
                    
                    if (data == nil) {
                        completionHandler(nil, error)
                        return
                    }
                    let enviarOTP = UtilsResponseService.sharedInstance.parseSendOTP(data: data!)!
                    
                    if (!enviarOTP.respuestaTransaccion.esExitosa) {
                        completionHandler(nil, enviarOTP.respuestaTransaccion.errorEntransaccion)
                        return
                    }
                    completionHandler(enviarOTP, error)
                })
            } else {
                completionHandler(nil, error)
            }
        })
    }
    
    public func validarOTP(guidOTP: String, datosOTP: String, completionHandler: @escaping (ValidarOTP?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["GuidOTP"] = guidOTP
        data["OTP"] = datosOTP
        
        let path = "ValidarOTP"
        _olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let validarOTP = UtilsResponseService.sharedInstance.parseValidateOTP(data: data!)!
            if (!validarOTP.respuestaTransaccion.esExitosa) {
                completionHandler(nil, validarOTP.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(validarOTP, error)
        })
    }
    
    public func guardarCiudadano(ciudadano: CiudadanoIn, completionHandler: @escaping (GuardarCiudadano?, ErrorEntransaccion?) -> ()) {
        
        var userId:String? = nil
        if (!ciudadano.guidCiu.isEmpty) {
            userId = ciudadano.guidCiu
        }
        
        var data:Dictionary<String, Any> = Dictionary()
        data["GuidCiu"] = userId
        data["GuidConv"] = ciudadano.guidConv
        data["TipoDoc"] = ciudadano.tipoDoc
        data["NumDoc"] = ciudadano.numDoc
        data["Email"] = ciudadano.email
        data["CodPais"] = ciudadano.codPais
        data["Celular"] = ciudadano.celular
        data["DatosAdi"] = ciudadano.datosAdicionales
        
        let path = "GuardarCiudadano"
        _olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let guardarCiudadano = UtilsResponseService.sharedInstance.parseSaveResident(data: data!)!
            if (!guardarCiudadano.respuestaTransaccion.esExitosa) {
                completionHandler(nil, guardarCiudadano.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(guardarCiudadano, error)
        })
    }
    
    public func consultarCiudadano(guidCiu: String, completionHandler: @escaping (ConsultarCiudadano?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["GuidCiudadano"] = guidCiu
        
        let path = "ConsultarCiudadano"
        _olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let consultarCiudadano = UtilsResponseService.sharedInstance.parseConsultResident(data: data!)!
            if (!consultarCiudadano.respuestaTransaccion.esExitosa) {
                completionHandler(nil, consultarCiudadano.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(consultarCiudadano, error)
        })
    }
    
    public func guardarBiometria(validarBiometria: ValidarBiometriaIn, completionHandler: @escaping (GuardarBiometria?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["GuidCiu"] = validarBiometria.guiCiudadano
        data["IdServicio"] = validarBiometria.idServicio
        data["SubTipo"] = validarBiometria.subtipo
        data["Valor"] = validarBiometria.biometria
        data["Formato"] = validarBiometria.formato
        data["DatosAdi"] = validarBiometria.datosAdicionales
        data["Usuario"] = validarBiometria.usuario
        
        let path = "GuardarBiometria"
        
        let subtype1 = ReconoSerConstants.Name.Biometrics.nameSubserviceFront
        let subtype2 = ReconoSerConstants.Name.Biometrics.nameSubserviceBack
        let subtype3 = ReconoSerConstants.Name.Biometrics.nameSubserviceBarcode
        let subtype4 = ReconoSerConstants.Name.Biometrics.nameSubserviceFacialFront
        
        var typeService:String = ""
        if (validarBiometria.subtipo.range(of: subtype1) != nil || validarBiometria.subtipo.range(of: subtype2) != nil || validarBiometria.subtipo.range(of: subtype3) != nil) {
            typeService = ReconoSerConstants.Name.Biometrics.typeServiceDocument
        } else if (validarBiometria.subtipo.range(of: subtype4) != nil) {
            typeService = ReconoSerConstants.Name.Biometrics.typeServiceFacial
        }
        
        UtilsResponseService.sharedInstance.validarConvenio(nameService: ReconoSerConstants.Name.Biometrics.nameService, typeService:typeService , subtypeService: validarBiometria.subtipo, completionHandler: { (isSuccess, error) in
            if (isSuccess) {
                self._olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
                    
                    if (data == nil) {
                        completionHandler(nil, error)
                        return
                    }
                    let guardarBiometria = UtilsResponseService.sharedInstance.parseSaveBiometry(data: data!)!
                    if (!guardarBiometria.respuestaTransaccion.esExitosa) {
                        completionHandler(nil, guardarBiometria.respuestaTransaccion.errorEntransaccion)
                        return
                    }
                    completionHandler(guardarBiometria, error)
                })
            } else {
                completionHandler(nil, error)
            }
        })
    }
    
    public func validarBiometria(validarBiometria: ValidarBiometriaIn, completionHandler: @escaping (ValidarBiometria?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["GuidCiudadano"] = validarBiometria.guiCiudadano
        data["IdServicio"] = validarBiometria.idServicio
        data["Biometria"] = validarBiometria.biometria
        data["Formato"] = validarBiometria.formato
        
        let path = "ValidarBiometria"
        _olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let validarBiometria = UtilsResponseService.sharedInstance.parseValidateBiometry(data: data!)!
            if (!validarBiometria.respuestaTransaccion.esExitosa) {
                completionHandler(nil, validarBiometria.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(validarBiometria, error)
        })
    }
    
    public func guardarlogError(error: GuardarLogErrorIn, completionHandler: @escaping (GuardarLogError?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["GuidConv"] = error.guidConv
        data["Texto"] = error.texto
        data["Componente"] = error.componente
        data["Usuario"] = error.usuario
        
        let path = "GuardarlogError"
        _olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let guardarLogError = UtilsResponseService.sharedInstance.parseSaveLogError(data: data!)!
            if (!guardarLogError.esExitosa) {
                completionHandler(nil, guardarLogError.errorEntransaccion)
                return
            }
            completionHandler(guardarLogError, error)
        })
    }
    
    public func solicitarPreguntasDemograficas(guidCiu: String, completionHandler: @escaping (SolicitarPreguntaDemografica?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["GuidCiudadano"] = guidCiu
        
        let path = "SolicitarPreguntasDemograficas"
        _olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let solicitarPreguntaDemografica = UtilsResponseService.sharedInstance.parseRequestQuestions(data: data!)!
            if (!solicitarPreguntaDemografica.respuestaTransaccion.esExitosa) {
                completionHandler(nil, solicitarPreguntaDemografica.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(solicitarPreguntaDemografica, error)
        })
    }
    
    public func validarRespuestaDemograficas(guidCiu: String, idCuestionario:String, registroCuestionario:Int, respuestas: [RespuestasIn], completionHandler: @escaping (ValidarRespuestaDemografica?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        var dataAnswers = [Dictionary<String, Any>]()
        
        for tempAnswer in respuestas {
            var answer:Dictionary<String, Any> = Dictionary()
            answer["IdPregunta"] = tempAnswer.idPregunta
            answer["IdRespuesta"] = tempAnswer.idRespuesta
            dataAnswers.append(answer)
        }
        
        data["GuidCiudadano"] = guidCiu
        data["IdCuestionario"] = idCuestionario
        data["RegistroCuestionario"] = registroCuestionario
        data["Respuestas"] = dataAnswers
        
        var request:Dictionary<String, Any> = Dictionary()
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: data,
            options: JSONSerialization.WritingOptions.prettyPrinted) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
            request["Repuestas"] = theJSONText
        }
        
        let path = "ValidarRespuestaDemografica"
        _olimpiaPost(path: path, parameters: request, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let validarRespuestaDemografica = UtilsResponseService.sharedInstance.parseValidateAnswers(data: data!)!
            if (!validarRespuestaDemografica.respuestaTransaccion.esExitosa) {
                completionHandler(nil, validarRespuestaDemografica.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(validarRespuestaDemografica, error)
        })
    }
    
    public func guardarDocumento(tipoDocumento: String, barcode: String, completionHandler: @escaping (GuardarDocumento?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["TipoDocumento"] = tipoDocumento
        data["Barcode"] = barcode
        
        let path = "GuardarDocumento"
        _olimpiaPost(path: path, parameters: data, completionHandler:{(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let guardarDocumento = UtilsResponseService.sharedInstance.parseSaveDocument(data: data!)
            if (!guardarDocumento!.respuestaTransaccion.esExitosa) {
                completionHandler(nil, guardarDocumento!.respuestaTransaccion.errorEntransaccion)
                return
            }
            completionHandler(guardarDocumento, error)
        })
    }
    
    internal func uploadS3(imageBase64: String, defaultFolder: String, completionHandler: @escaping (NSDictionary?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["imageBase64"] = imageBase64
        
        if (!defaultFolder.isEmpty) {
            data["defaultFolder"] = defaultFolder
        }
        
        let path = "rekognition"
        _reconoSerPost(path: path, parameters: data, completionHandler: completionHandler)
    }
    
    internal func compareFaces(sourceImage: String, targetImage: String, completionHandler: @escaping (CompararRostro?, ErrorEntransaccion?) -> ()) {
        
        var data:Dictionary<String, Any> = Dictionary()
        data["SourceImage"] = sourceImage
        data["TargetImage"] = targetImage
        
        let path = "compare-faces"
        _reconoSerPost(path: path, parameters: data, completionHandler: {(data, error) in
            
            if (data == nil) {
                completionHandler(nil, error)
                return
            }
            let compararRostro = UtilsResponseService.sharedInstance.parseCompareFaces(data: data!)
            completionHandler(compararRostro, error)
        })
    }
    
    internal func _reconoSerPost(path:String, parameters:Dictionary<String, Any>, completionHandler: @escaping (NSDictionary?, ErrorEntransaccion?) -> ()) {
        
        _post(url: ReconoSerConstants.baseUrl + path, parameters: parameters, completionHandler: completionHandler)
    }
    
    internal func _olimpiaPost(path:String, parameters:Dictionary<String, Any>, completionHandler: @escaping (NSDictionary?, ErrorEntransaccion?) -> ()) {
        
        _post(url: ReconoSerConstants.olimpiaBaseUrl + path, parameters: parameters, completionHandler: completionHandler)
    }
    
    internal func _post(url:String, parameters:Dictionary<String, Any>, completionHandler: @escaping (NSDictionary?, ErrorEntransaccion?) -> ()) {
        
        print("Parameters: ", parameters)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Response status: \(status)")
                }
                //to get JSON return value
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    print("Response: ", JSON)
                    completionHandler(JSON, nil)
                } else {
                    if let error = response.result.error {
                        
                        let errorStr = ReconoSerConstants.Error.ERROR_SERVER + ": " + error.localizedDescription
                        completionHandler(nil, ErrorEntransaccion(codigo: ReconoSerConstants.Error.ERROR_R103, descripcion: errorStr))
                    }
                }
        }
    }
}

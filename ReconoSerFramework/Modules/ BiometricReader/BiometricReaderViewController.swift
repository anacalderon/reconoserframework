//
//  BiometricReaderViewController.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import AVFoundation
import UIKit
import FirebaseMLVision

protocol BiometricReaderView: class {
    func displayViewInformation(model: BiometricReaderModel)
    func displayCurrentPolicies(model: BiometricReaderModel)
    func takePhoto()
    func displayViewValidationSuccess()
    func displayViewBack()
    func showLoader()
    func hideLoader()
}

public class BiometricReaderViewController: UIViewController {
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var img_mask: UIImageView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var containerLoading: UIView!
    
    fileprivate var stillImageOutput: AVCapturePhotoOutput?
    fileprivate var input: AVCaptureDeviceInput!
    fileprivate var captureSession: AVCaptureSession!
    fileprivate var camera: AVCaptureDevice!
    fileprivate var currentModel: BiometricReaderModel?
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer!
    
    var delegate: BiometricReaderDelegate?
    var imageToCompare: UIImage?
    var presenter: BiometricReaderPresenter!
    
    private lazy var annotationOverlayView: UIView = {
        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOverlayView
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        configurePresenter()
        setUpAnnotationOverlayView()
        setMaskNormal()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        setupCamera()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        captureSession.stopRunning()
        
        for output in captureSession.outputs {
            captureSession.removeOutput(output)
            captureSession.removeOutput(stillImageOutput!)
        }
        
        for input in captureSession.inputs {
            captureSession.removeInput(input)
        }
        captureSession = nil
        camera = nil
    }
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setUpAnnotationOverlayView() {
        previewView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: previewView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: previewView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: previewView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: previewView.bottomAnchor),
            ])
    }
    
    private func removeDetectionAnnotations() {
        for annotationView in annotationOverlayView.subviews {
            annotationView.removeFromSuperview()
        }
    }
    
    private func setupCamera(){
        captureSession = AVCaptureSession()
        
        camera = AVCaptureDevice.default(
            .builtInWideAngleCamera,
            for: .video,
            position: .front)
        
        do {
            input = try AVCaptureDeviceInput(device: camera)
        } catch let error as NSError {
            print(error)
        }
        
        if captureSession.canAddInput(input) {
            captureSession.addInput(input)
        }
        
        let output = AVCaptureVideoDataOutput()
        stillImageOutput = AVCapturePhotoOutput()
        
        output.videoSettings =
            [(kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA]
        
        if captureSession.canAddOutput(output) {
            captureSession.addOutput(output)
            captureSession.addOutput(stillImageOutput!)
        }
        captureSession.commitConfiguration()
        
        let queue = DispatchQueue(label: "output.queue")
        output.setSampleBufferDelegate(self, queue: queue)
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.bounds
        previewLayer.videoGravity = .resizeAspectFill
        previewView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    // MARK: On-Device Detection
    
    private func detectFacesOnDevice(in image: VisionImage, width: CGFloat, height: CGFloat) {
        let options = VisionFaceDetectorOptions()
        options.landmarkMode = .all
        options.isTrackingEnabled = true
        options.classificationMode = .all
        
        let vision: Vision = Vision.vision();
        let faceDetector = vision.faceDetector(options: options)
        
        faceDetector.process(image) { features, error in
            if let error = error {
                print("Error", error.localizedDescription)
                return
            }
            
            guard error == nil, let features = features, !features.isEmpty else {
                print("On-Device face detector returned no results.")
                self.presenter.onMissingFeatures()
                return
            }
            
            let error = Localization.faceError
            let message = self.infoLabel.text
            if (features.count > 1) {
                if (message?.range(of: error) != nil) {
                    return
                }
                self.infoLabel.text = (message ?? "").appending(error)
                self.presenter.onMissingFeatures()
                return
            } else {
                let range = message?.range(of: error)
                if (range != nil) {
                    self.infoLabel.text = message?.replacingCharacters(in: range!, with: "")
                }
            }
            
            for face in features {
                self.presenter.onFaceFeature(facefeature: face)
                break
            }
        }
    }
    
    private func setFaceCenterText() {
        infoLabel.text = Localization.centerFace
        setMaskNormal()
    }
}

extension BiometricReaderViewController {
    
    private func configurePresenter() {
        if presenter == nil {
            presenter = BiometricReaderPresenter(view: self)
        }
        presenter.presentViewInformation()
    }
    
    private func setMaskNormal() {
        setMask(imageName: "bg_face_camera")
    }
    
    private func setMaskRight() {
        setMask(imageName: "bg_face_der_camera")
    }
    
    private func setMaskLeft() {
        setMask(imageName: "bg_face_izq_camera")
    }
    
    private func setMaskOk() {
        setMask(imageName: "bg_face_ok_camera")
    }
    
    private func setMask(imageName: String) {
        if UIDevice.current.screenFamily == .iPhonePlus {
            img_mask.image = UIImage(named: imageName + "_plus")
        } else if UIDevice.current.screenFamily == .iPhoneStandard  {
            img_mask.image = UIImage(named: imageName + "_normal")
        } else {
            img_mask.image = UIImage(named: imageName + "_x")
        }
    }
    
    public class func initFromStoryboard(guidAgreement:String, dataConv:String, completionHandler: @escaping (BiometricReaderViewController?, ErrorEntransaccion?) -> ()) {
        
        let presenter = PublicBiometricReaderPresenter()
        presenter.validateAgreement(guidAgreement: guidAgreement, dataConv: dataConv, completionHandler: {(isSuccess, error) in
            
            if (isSuccess) {
                let storyboard = UIStoryboard(name: ReconoSerConstants.Name.BiometricReaderViewController.storyboard, bundle: Bundle(for: self))
                guard let viewController = storyboard.instantiateViewController(withIdentifier: ReconoSerConstants.Name.BiometricReaderViewController.idIdentifier) as? BiometricReaderViewController else {
                    fatalError("Unable to instantiate viewController \(ReconoSerConstants.Name.BiometricReaderViewController.idIdentifier) from storyboard \(ReconoSerConstants.Name.BiometricReaderViewController.storyboard)")
                }
                completionHandler(viewController, nil)
            } else {
                completionHandler(nil, error)
            }
        })
    }
    
    private func displayViewShowPhoto(imageData: Data) {
        
        if let image = UIImage(data: imageData) {
            let square = image.size.width < image.size.height ? CGSize(width: image.size.width, height: image.size.width) : CGSize(width: image.size.height, height: image.size.height)
            let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
            imageView.contentMode = UIView.ContentMode.scaleAspectFill
            imageView.image = image
            imageView.layer.masksToBounds = true
            UIGraphicsBeginImageContext(imageView.bounds.size)
            imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            presenter.timer = nil
            presenter.presentDismiss()
            
            let currentImage = result?.toBase64()
            
            if (currentImage == nil) {
                let errorImage = ErrorEntransaccion(codigo: ReconoSerConstants.Error.ERROR_R106, descripcion: ReconoSerConstants.Error.ERROR_IMAGE_PROCESS)
                self._sendData(image: nil, imageData: nil, result: nil, error: errorImage)
                return
            }
            
            if (imageToCompare == nil) {
                self.showLoader()
                self.presenter.uploadImage(image: currentImage!, completionHandler: {(data, error) in
                    self.hideLoader()
                    self._onUploadImage(image: result, imageData: imageData, result: nil, data: data, error: error)
                })
                return
            }
            
            let imageToCompare64 = self.imageToCompare?.toBase64()
            self.showLoader()
            presenter.compareFaces(image: currentImage!, imageToCompare: imageToCompare64!, completionHandler: {(dataCompareFace, error) in
                self.hideLoader()
                
                if (error != nil) {
                    self._sendData(image: nil, imageData: nil, result: nil, error: error)
                    return
                }
                
                self.showLoader()
                self.presenter.uploadImage(image: currentImage!, completionHandler: {(dataUploadImage, error) in
                    self.hideLoader()
                    self._onUploadImage(image: result, imageData: imageData, result: dataCompareFace, data: dataUploadImage, error: error)
                })
            })
        }
    }
    
    private func _onUploadImage (image:UIImage?, imageData:Data?, result: CompararRostro?, data: NSDictionary?, error: ErrorEntransaccion?) {
        if (error != nil) {
            let errorImage = ErrorEntransaccion(codigo: ReconoSerConstants.Error.ERROR_R105, descripcion: ReconoSerConstants.Error.ERROR_IMAGE_SAVE)
            self._sendData(image: nil, imageData: nil, result: nil, error: errorImage)
            return
        }
        
        self._sendData(image: image, imageData: imageData, result: result, error: nil)
    }
    
    private func _sendData(image: UIImage?, imageData: Data?, result: CompararRostro?, error: ErrorEntransaccion?) {
        self.dismiss(animated: true) {
            self.delegate?.onScanFace(image, imageData:imageData, result: result, error: error)
        }
    }
}

extension BiometricReaderViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            print("Failed to get image buffer from sample buffer.")
            return
        }
        let visionImage = VisionImage(buffer: sampleBuffer)
        let metadata = VisionImageMetadata()
        let visionOrientation = UIUtilities.visionImageOrientation(from: UIUtilities.imageOrientation(fromDevicePosition: .front))
        metadata.orientation = visionOrientation
        visionImage.metadata = metadata
        let imageWidth = CGFloat(CVPixelBufferGetWidth(imageBuffer))
        let imageHeight = CGFloat(CVPixelBufferGetHeight(imageBuffer))
        detectFacesOnDevice(in: visionImage, width: imageWidth, height: imageHeight)
    }
}

extension BiometricReaderViewController: BiometricReaderView {
    
    internal func displayViewBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    internal func displayViewValidationSuccess() {
        setMaskOk()
        infoLabel.text = Localization.isOK
    }
    
    internal func takePhoto() {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 640,
                             kCVPixelBufferHeightKey as String: 640]
        settings.previewPhotoFormat = previewFormat
        self.stillImageOutput!.capturePhoto(with: settings, delegate: self)
    }
    
    internal func displayCurrentPolicies(model: BiometricReaderModel) {
        switch model.currentPolicies {
        case .blink:
            infoLabel.text = Localization.blinksSoftly
            setMaskNormal()
        case .smile:
            infoLabel.text = Localization.smile
            setMaskNormal()
        case .turnRight:
            infoLabel.text = Localization.keepTurning
        case .turnLeft:
            infoLabel.text = Localization.keepTurning
        case .none:
            infoLabel.text = Localization.noFaceDetected
            setMaskNormal()
        case .center:
            self.setFaceCenterText()
        case .turnMidLeft:
            infoLabel.text = Localization.turnHeadLeft
            setMaskLeft()
        case .turnMidRight:
            infoLabel.text = Localization.turnHeadRight
            setMaskRight()
        }
    }
    
    internal func displayViewInformation(model: BiometricReaderModel) {
        currentModel = model
        infoLabel.text = currentModel?.statusFaceDetect
    }
    
    func showLoader() {
        containerLoading.isHidden = false
        loading.startAnimating()
    }
    
    func hideLoader() {
        containerLoading.isHidden = true
        loading.stopAnimating()
    }
}

extension BiometricReaderViewController: AVCapturePhotoCaptureDelegate {
    
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        let imageData = photo.fileDataRepresentation()
        self.displayViewShowPhoto(imageData: imageData!)
    }
}

//
//  CameraUserValidationModel.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import AVFoundation

struct BiometricReaderModel {
    var title: String
    var statusFaceDetect: String
    var isOpenEyes: Bool
    var isPhototaken: Bool
    var timerEyes: Timer
    var cameraFlashConfig: AVCaptureDevice.FlashMode
    var isUpdateTime: Bool
    var isFront: Bool
    var previewCamFrame: CGRect
    var currentPolicies: RuleCameraValidation
}

//
//  CameraUserValidationPresenter.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation
import AVFoundation
import FirebaseMLVision
import UIKit

enum RuleCameraValidation {
    case turnLeft
    case turnMidLeft
    case turnRight
    case turnMidRight
    case blink
    case smile
    case center
    case none
}

internal protocol BiometricReaderViewPresenter: class {
    init(view: BiometricReaderView)
    func presentViewInformation()
    func presentBackView()
    func presentDismiss()
    func compareFaces(image: String, imageToCompare: String, completionHandler: @escaping (CompararRostro?, ErrorEntransaccion?) -> ())
    func uploadImage(image: String, completionHandler: @escaping (NSDictionary?, ErrorEntransaccion?) -> ())
}

internal protocol BiometricReaderValidationPresenter: class {
    func validateAgreement(guidAgreement:String, dataConv:String, completionHandler: @escaping (Bool, ErrorEntransaccion?)->())
}

internal class PublicBiometricReaderPresenter: NSObject, BiometricReaderValidationPresenter {
    
    public override init() {
        
    }
    
    func validateAgreement(guidAgreement: String, dataConv: String, completionHandler: @escaping (Bool, ErrorEntransaccion?) -> ()) {
        
        ServiciosOlimpia.sharedInstance.consultarConvenio(guidConv: guidAgreement, datosConv: dataConv, completionHandler: {(consultarConvenio, error) in
            
            if (error != nil) {
                completionHandler(false, error)
                return
            }
            
            let subtypeService = ReconoSerConstants.Name.Biometrics.nameSubserviceFacialFront
            
            UtilsResponseService.sharedInstance.validarConvenio(nameService: ReconoSerConstants.Name.Biometrics.nameService, typeService:ReconoSerConstants.Name.Biometrics.typeServiceFacial , subtypeService: subtypeService, completionHandler: { (isSuccess, error) in
                if (isSuccess) {
                    completionHandler(true, nil)
                } else {
                    completionHandler(false, error)
                }
            })
        })
    }
}

internal class BiometricReaderPresenter: NSObject, BiometricReaderViewPresenter {
    unowned var view: BiometricReaderView
    private let kTitle = Localization.noFaceDetected
    private var currentPolicieFaceValidate: RuleCameraValidation = .none
    private var currentCameraValidationModel: BiometricReaderModel?
    var timer: Timer?
    private var arrayPoliciesValidation: [RuleCameraValidation] = []
    
    private var isPhotoTaken = false
    private var isBack = false
    
    private let kStatusFaceNoDetect = Localization.noFaceDetected
    
    required init(view: BiometricReaderView) {
        self.view = view
    }
    
    internal func presentDismiss() {
        isBack = true
    }
    
    internal func presentBackView() {
        isBack = true
        self.view.displayViewBack()
    }
    
    internal func presentViewInformation() {
        let model = BiometricReaderModel(title: kTitle, statusFaceDetect: kStatusFaceNoDetect, isOpenEyes: false, isPhototaken: false, timerEyes: Timer(), cameraFlashConfig: .auto, isUpdateTime: false, isFront: true, previewCamFrame: CGRect(x: 0, y: 0, width: 0, height: 0), currentPolicies: .none)
        
        currentCameraValidationModel = model
        view.displayViewInformation(model: currentCameraValidationModel!)
    }
    
    private func setRulesRandom(numberValidations: Int) {
        var arrayAllPoliciesFaceValidation = [RuleCameraValidation.smile, RuleCameraValidation.blink, RuleCameraValidation.turnLeft, RuleCameraValidation.turnRight]
        arrayPoliciesValidation = []
        for _ in 0 ..< numberValidations {
            let randomIndex = Int(arc4random_uniform(UInt32(arrayAllPoliciesFaceValidation.count)))
            if arrayAllPoliciesFaceValidation[randomIndex] == .turnLeft {
                arrayPoliciesValidation.append(RuleCameraValidation.turnMidLeft)
            } else if arrayAllPoliciesFaceValidation[randomIndex] == .turnRight {
                arrayPoliciesValidation.append(RuleCameraValidation.turnMidRight)
            }
            
            arrayPoliciesValidation.append(arrayAllPoliciesFaceValidation[randomIndex])
            if arrayAllPoliciesFaceValidation[randomIndex] == .turnLeft || arrayAllPoliciesFaceValidation[randomIndex] == .turnRight {
                arrayPoliciesValidation.append(RuleCameraValidation.center)
            }
            
            arrayAllPoliciesFaceValidation.remove(at: randomIndex)
        }
        
        arrayPoliciesValidation.insert(RuleCameraValidation.center, at: 0)
        arrayPoliciesValidation.append(RuleCameraValidation.center)
        getNextPolicieFaceValidation(policiesValidations: arrayPoliciesValidation)
    }
    
    private func getNextPolicieFaceValidation(policiesValidations: [RuleCameraValidation]) {
        if policiesValidations.isEmpty {
            
        } else {
            let caseValidate = policiesValidations.first
            switch caseValidate! {
            case RuleCameraValidation.blink:
                currentCameraValidationModel?.currentPolicies = .blink
            case RuleCameraValidation.smile:
                currentCameraValidationModel?.currentPolicies = .smile
            case RuleCameraValidation.turnRight:
                currentCameraValidationModel?.currentPolicies = .turnRight
            case RuleCameraValidation.turnLeft:
                currentCameraValidationModel?.currentPolicies = .turnLeft
            case .none:
                currentCameraValidationModel?.currentPolicies = .none
            case .center:
                currentCameraValidationModel?.currentPolicies = .center
            case .turnMidLeft:
                currentCameraValidationModel?.currentPolicies = .turnMidLeft
            case .turnMidRight:
                currentCameraValidationModel?.currentPolicies = .turnMidRight
            }
        }
    }
    
    private func setTurnLeftFaceRule(feature: VisionFace!) {
        if feature.headEulerAngleY < -23 {
            if self.isBack == false {
                view.displayViewValidationSuccess()
                if !arrayPoliciesValidation.isEmpty {
                    self.arrayPoliciesValidation.removeFirst()
                }
                
                self.getNextPolicieFaceValidation(policiesValidations: self.arrayPoliciesValidation)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
                    guard let strongSelf = self else { return }
                    // Do task in main queue
                    if self?.isBack == false {
                        strongSelf.view.displayCurrentPolicies(model: (strongSelf.currentCameraValidationModel!))
                    }
                }
            }
        }
    }
    
    private func setTurnRightFaceRule(feature: VisionFace!) {
        if feature.headEulerAngleY > 23 {
            if self.isBack == false {
                view.displayViewValidationSuccess()
                if !arrayPoliciesValidation.isEmpty {
                    self.arrayPoliciesValidation.removeFirst()
                }
                self.getNextPolicieFaceValidation(policiesValidations:self.arrayPoliciesValidation)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self]  in
                    guard let strongSelf = self else { return }
                    // Do task in main queue
                    if  strongSelf.isPhotoTaken == false {
                        strongSelf.view.displayCurrentPolicies(model: (strongSelf.currentCameraValidationModel!))
                    }
                }
            }
        }
    }
    
    private func setBlinkEyesRule(feature: VisionFace!, currentModel: BiometricReaderModel) {
        if feature.rightEyeOpenProbability < 0.3 && feature.leftEyeOpenProbability < 0.3 {
            if self.isBack == false {
                view.displayViewValidationSuccess()
                if !arrayPoliciesValidation.isEmpty {
                    self.arrayPoliciesValidation.removeFirst()
                }
                
                self.getNextPolicieFaceValidation(policiesValidations:self.arrayPoliciesValidation)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self]  in
                    guard let strongSelf = self else { return }
                    // Do task in main queue
                    if  strongSelf.isPhotoTaken == false {
                        
                        strongSelf.view.displayCurrentPolicies(model: (strongSelf.currentCameraValidationModel!))
                    }
                }
            }
        }
    }
    
    private func setSmileRule(feature: VisionFace!, currentModel: BiometricReaderModel) {
        if feature.smilingProbability >= 0.6 {
            view.displayViewValidationSuccess()
            if !arrayPoliciesValidation.isEmpty {
                self.arrayPoliciesValidation.removeFirst()
            }
            
            self.getNextPolicieFaceValidation(policiesValidations:self.arrayPoliciesValidation)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self]  in
                guard let strongSelf = self else { return }
                // Do task in main queue
                if  strongSelf.isPhotoTaken == false {
                    if self?.isBack == false {
                        strongSelf.view.displayCurrentPolicies(model: (strongSelf.currentCameraValidationModel!))
                    }
                }
            }
        }
    }
    
    private func setTurnMidRightFaceRule(feature: VisionFace!, currentModel: BiometricReaderModel) {
        if feature.headEulerAngleY >= 12 && feature.headEulerAngleY <= 30 {
            if !arrayPoliciesValidation.isEmpty {
                self.arrayPoliciesValidation.removeFirst()
            }
            getNextPolicieFaceValidation(policiesValidations: arrayPoliciesValidation)
            view.displayCurrentPolicies(model: currentCameraValidationModel!)
        }
    }
    
    private func setTurnMidLeftFaceRule(feature: VisionFace!, currentModel: BiometricReaderModel) {
        if feature.headEulerAngleY >= -30 && feature.headEulerAngleY <= -12 {
            if !arrayPoliciesValidation.isEmpty {
                self.arrayPoliciesValidation.removeFirst()
            }
            
            getNextPolicieFaceValidation(policiesValidations: arrayPoliciesValidation)
            view.displayCurrentPolicies(model: currentCameraValidationModel!)
        }
    }
    
    private func setCenterRule(feature: VisionFace!, currentModel: BiometricReaderModel) {
        if feature.headEulerAngleY >= -10 && feature.headEulerAngleY <= 10 && feature.leftEyeOpenProbability > 0.8 && feature.rightEyeOpenProbability > 0.8 {
            if arrayPoliciesValidation.isEmpty && isPhotoTaken == false {
                
                if timer != nil {
                    timer?.invalidate()
                    timer = nil
                }
                
                view.takePhoto()
                isPhotoTaken = true
                return
            }
            
            if arrayPoliciesValidation.isEmpty {
                return
            }
            
            arrayPoliciesValidation.removeFirst()
            getNextPolicieFaceValidation(policiesValidations: arrayPoliciesValidation)
            if isBack == false {
                view.displayCurrentPolicies(model: currentCameraValidationModel!)
            }
        }
    }
    
    func compareFaces(image: String, imageToCompare: String, completionHandler: @escaping (CompararRostro?, ErrorEntransaccion?) -> ()) {
        
        self.view.showLoader()
        ServiciosOlimpia.sharedInstance.compareFaces(sourceImage: image, targetImage: imageToCompare, completionHandler: {(data, error) in
            self.view.hideLoader()
            completionHandler(data, error)
        })
    }
    
    func uploadImage(image: String, completionHandler: @escaping (NSDictionary?, ErrorEntransaccion?) -> ()) {
        
        self.view.showLoader()
        ServiciosOlimpia.sharedInstance.uploadS3(imageBase64: image, defaultFolder: "", completionHandler: {(data, error) in
            self.view.hideLoader()
            completionHandler(data, error)
        })
    }
}

extension BiometricReaderPresenter {
    
    @objc private func update() {
        if timer != nil {
            if isBack == false {
                currentCameraValidationModel?.currentPolicies = .none
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self]  in
                    guard let strongSelf = self else { return }
                    // Do task in main queue
                    strongSelf.view.displayCurrentPolicies(model: (strongSelf.currentCameraValidationModel!))
                }
            }
        }
    }
    
    internal func onMissingFeatures() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
    }
    
    internal func onFaceFeature(facefeature: VisionFace) {
        
        let rules = currentCameraValidationModel?.currentPolicies
        switch rules! {
        case RuleCameraValidation.blink:
            setBlinkEyesRule(feature: facefeature, currentModel: currentCameraValidationModel!)
        case RuleCameraValidation.smile:
            setSmileRule(feature: facefeature, currentModel: currentCameraValidationModel!)
        case RuleCameraValidation.turnLeft:
            setTurnLeftFaceRule(feature: facefeature)
        case RuleCameraValidation.turnRight:
            setTurnRightFaceRule(feature: facefeature)
        case RuleCameraValidation.none:
            setRulesRandom(numberValidations: 2)
        case .center:
            setCenterRule(feature: facefeature, currentModel: currentCameraValidationModel!)
        case .turnMidLeft:
            setTurnMidLeftFaceRule(feature: facefeature, currentModel: currentCameraValidationModel!)
        case .turnMidRight:
            setTurnMidRightFaceRule(feature: facefeature, currentModel: currentCameraValidationModel!)
        }
    }
}

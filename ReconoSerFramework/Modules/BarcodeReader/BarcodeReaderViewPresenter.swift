//
//  BarcodeReaderViewPresenter.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

protocol BarcodeReaderValidationPresenter: class {
    func validateAgreement(guidAgreement:String, dataConv:String, completionHandler: @escaping (Bool, ErrorEntransaccion?)->())
}

internal class PublicBarcodeReaderPresenter: BarcodeReaderValidationPresenter {
    
    func validateAgreement(guidAgreement: String, dataConv: String, completionHandler: @escaping (Bool, ErrorEntransaccion?) -> ()) {
        
        ServiciosOlimpia.sharedInstance.consultarConvenio(guidConv: guidAgreement, datosConv: dataConv, completionHandler: {(consultarConvenio, error) in
            
            if (error != nil) {
                completionHandler(false, error)
                return
            }
            
            let subtypeService = ReconoSerConstants.Name.BarcodeReaderViewController.nameSubserviceBarcode
            
            UtilsResponseService.sharedInstance.validarConvenio(nameService: ReconoSerConstants.Name.DocumentReaderViewController.nameService, typeService:ReconoSerConstants.Name.DocumentReaderViewController.typeService , subtypeService: subtypeService, completionHandler: { (isSuccess, error) in
                if (isSuccess) {
                    completionHandler(true, nil)
                } else {
                    completionHandler(false, error)
                }
            })
        })
    }
}

//
//  BarcodeReaderViewController.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import UIKit
import ZXingObjC

public class BarcodeReaderViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var scanView: UIView?
    @IBOutlet weak var img_mask: UIImageView!
    @IBOutlet weak var img_close: UIImageView!
    
    fileprivate var capture: ZXCapture?
    var delegate: BarcodeReaderDelegate?
    fileprivate var resultLabel:UILabel?
    
    fileprivate var isScanning: Bool?
    fileprivate var isFirstApplyOrientation: Bool?
    fileprivate var captureSizeTransform: CGAffineTransform?
    
    // MARK: Life Circles
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setMaskNormal()
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirstApplyOrientation == true { return }
        isFirstApplyOrientation = true
        applyOrientation()
    }
    
    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            // do nothing
        }) { [weak self] (context) in
            guard let weakSelf = self else { return }
            weakSelf.applyOrientation()
        }
    }
    
    @IBAction func closeView(_ sender: Any) {
        isScanning = false
        capture?.layer.removeFromSuperlayer()
        capture?.stop()
        dismiss(animated: true, completion: nil)
    }
}

// MARK: Helpers
extension BarcodeReaderViewController {
    private func setup() {
        isScanning = false
        isFirstApplyOrientation = false
        
        capture = ZXCapture()
        guard let _capture = capture else { return }
        _capture.camera = _capture.back()
        _capture.focusMode =  .continuousAutoFocus
        _capture.delegate = self
        
        resultLabel = UILabel()
        resultLabel?.textColor = UIColor.white
        resultLabel?.textAlignment = .center
        resultLabel?.text = Localization.noDocumentDetected
        self.view.addSubview(resultLabel!)
        resultLabel?.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        resultLabel?.frame = CGRect(x: 10, y: 0, width: 22, height: self.view.bounds.height)
        
        self.view.layer.addSublayer(_capture.layer)
        guard let _scanView = scanView, let _img_mask = img_mask, let _resultLabel = resultLabel else { return }
        
        self.view.bringSubviewToFront(_scanView)
        self.view.bringSubviewToFront(_img_mask)
        self.view.bringSubviewToFront(_resultLabel)
        self.view.bringSubviewToFront(img_close)
        
        let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeView))
        gesture.numberOfTapsRequired = 1
        img_close.isUserInteractionEnabled = true
        img_close.addGestureRecognizer(gesture)
    }
    
    private func applyOrientation() {
        let orientation = UIApplication.shared.statusBarOrientation
        var captureRotation: Double
        var scanRectRotation: Double
        
        switch orientation {
        case .portrait:
            captureRotation = 0
            scanRectRotation = 180
            break
            
        case .landscapeLeft:
            captureRotation = 90
            scanRectRotation = 180
            break
            
        case .landscapeRight:
            captureRotation = 270
            scanRectRotation = 0
            break
            
        case .portraitUpsideDown:
            captureRotation = 180
            scanRectRotation = 270
            break
            
        default:
            captureRotation = 0
            scanRectRotation = 90
            break
        }
        
        applyRectOfInterest(orientation: orientation)
        
        let angleRadius = captureRotation / 180.0 * Double.pi
        let captureTranform = CGAffineTransform(rotationAngle: CGFloat(angleRadius))
        
        capture?.transform = captureTranform
        capture?.rotation = CGFloat(scanRectRotation)
        capture?.layer.frame = view.frame
    }
    
    private func applyRectOfInterest(orientation: UIInterfaceOrientation) {
        guard var transformedVideoRect = scanView?.frame,
            let cameraSessionPreset = capture?.sessionPreset
            else { return }
        
        var scaleVideoX, scaleVideoY: CGFloat
        var videoHeight, videoWidth: CGFloat
        
        // Currently support only for 1920x1080 || 1280x720
        if cameraSessionPreset == AVCaptureSession.Preset.hd1920x1080.rawValue {
            videoHeight = 1080.0
            videoWidth = 1920.0
        } else {
            videoHeight = 720.0
            videoWidth = 1280.0
        }
        
        if orientation == UIInterfaceOrientation.portrait {
            scaleVideoX = self.view.frame.width / videoHeight
            scaleVideoY = self.view.frame.height / videoWidth
            
            // Convert CGPoint under portrait mode to map with orientation of image
            // because the image will be cropped before rotate
            // reference: https://github.com/TheLevelUp/ZXingObjC/issues/222
            let realX = transformedVideoRect.origin.y;
            let realY = self.view.frame.size.width - transformedVideoRect.size.width - transformedVideoRect.origin.x;
            let realWidth = transformedVideoRect.size.height;
            let realHeight = transformedVideoRect.size.width;
            transformedVideoRect = CGRect(x: realX, y: realY, width: realWidth, height: realHeight);
            
        } else {
            scaleVideoX = self.view.frame.width / videoWidth
            scaleVideoY = self.view.frame.height / videoHeight
        }
        
        captureSizeTransform = CGAffineTransform(scaleX: 1.0/scaleVideoX, y: 1.0/scaleVideoY)
        guard let _captureSizeTransform = captureSizeTransform else { return }
        let transformRect = transformedVideoRect.applying(_captureSizeTransform)
        capture?.scanRect = transformRect
    }
    
    private func barcodeFormatToString(format: ZXBarcodeFormat) -> String {
        switch (format) {
            
        case kBarcodeFormatPDF417:
            return "PDF417"
            
        default:
            return "Unknown"
        }
    }
    
    private func setMaskNormal() {
        setMask(imageName: "bg_doc_code_camera")
    }
    
    private func setMask(imageName: String) {
        if UIDevice.current.screenFamily == .iPhonePlus {
            img_mask.image = UIImage(named: imageName + "_plus")
        } else if UIDevice.current.screenFamily == .iPhoneStandard  {
            img_mask.image = UIImage(named: imageName + "_normal")
        } else {
            img_mask.image = UIImage(named: imageName + "_x")
        }
    }
    
    public class func initFromStoryboard(guidAgreement:String, dataConv:String, completionHandler: @escaping (BarcodeReaderViewController?, ErrorEntransaccion?) -> ()) {
        
        _initFromStoryboard(guidAgreement: guidAgreement, dataConv: dataConv, completionHandler: completionHandler)
    }
    
    internal class func _initFromStoryboard(guidAgreement:String, dataConv:String, completionHandler: @escaping (BarcodeReaderViewController?, ErrorEntransaccion?) -> ()) {
        
        let presenter = PublicBarcodeReaderPresenter()
        presenter.validateAgreement(guidAgreement: guidAgreement, dataConv: dataConv, completionHandler: {(isSuccess, error) in
            
            if (isSuccess) {
                let storyboard = UIStoryboard(name: ReconoSerConstants.Name.DocumentReaderViewController.storyboard, bundle: Bundle(for: self))
                guard let viewController = storyboard.instantiateViewController(withIdentifier: ReconoSerConstants.Name.BarcodeReaderViewController.idIdentifier) as? BarcodeReaderViewController else {
                    fatalError("Unable to instantiate viewController \(ReconoSerConstants.Name.BarcodeReaderViewController.idIdentifier) from storyboard \(ReconoSerConstants.Name.BarcodeReaderViewController.storyboard)")
                }
                completionHandler(viewController, nil)
            } else {
                completionHandler(nil, error)
            }
        })
    }
}

// MARK: ZXCaptureDelegate
extension BarcodeReaderViewController: ZXCaptureDelegate {
    public func captureCameraIsReady(_ capture: ZXCapture!) {
        isScanning = true
    }
    
    public func captureResult(_ capture: ZXCapture!, result: ZXResult!) {
        guard let _result = result, isScanning == true else { return }
        isScanning = false
        capture?.layer.removeFromSuperlayer()
        capture?.stop()
        
        let text = _result.text ?? "Unknow"
        self.dismiss(animated: true, completion: {
            self.delegate?.onScanBarcode(result: text)
        })
    }
}

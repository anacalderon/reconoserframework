//
//  DocumentReaderModel.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import AVFoundation

struct DocumentReaderModel {
    var title: String
    var statusTextDetect: String
    var result: String
}

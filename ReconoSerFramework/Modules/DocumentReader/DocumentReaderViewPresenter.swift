//
//  DocumentReaderViewPresenter.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import AVFoundation
import UIKit
import FirebaseMLVision

protocol DocumentReaderViewPresenter: class {
    init(view: DocumentReaderView)
    func presentViewInformation()
    func presentBackView()
    func presentDismiss()
}

protocol DocumentReaderValidationPresenter: class {
    func validateAgreement(guidAgreement:String, dataConv:String, mode:DocumentReaderMode, completionHandler: @escaping (Bool, ErrorEntransaccion?)->())
}

internal class PublicDocumentReaderPresenter: NSObject, DocumentReaderValidationPresenter {
    
    public override init() {
    }
    
    func validateAgreement(guidAgreement: String, dataConv: String, mode: DocumentReaderMode, completionHandler: @escaping (Bool, ErrorEntransaccion?) -> ()) {
        
        ServiciosOlimpia.sharedInstance.consultarConvenio(guidConv: guidAgreement, datosConv: dataConv, completionHandler: {(consultarConvenio, error) in
            
            if (error != nil) {
                completionHandler(false, error)
                return
            }
            
            var subtypeService = ReconoSerConstants.Name.DocumentReaderViewController.nameSubserviceFront
            if (mode == DocumentReaderMode.back) {
                subtypeService = ReconoSerConstants.Name.DocumentReaderViewController.nameSubserviceBack
            }
            
            UtilsResponseService.sharedInstance.validarConvenio(nameService: ReconoSerConstants.Name.DocumentReaderViewController.nameService, typeService:ReconoSerConstants.Name.DocumentReaderViewController.typeService , subtypeService: subtypeService, completionHandler: { (isSuccess, error) in
                if (isSuccess) {
                    completionHandler(true, nil)
                } else {
                    completionHandler(false, error)
                }
            })
        })
    }
}

internal class DocumentReaderPresenter: NSObject, DocumentReaderViewPresenter {
    private var view: DocumentReaderView
    private let kTitle = Localization.noDocumentDetected
    private var isPhotoTaken = false
    private var isBack = false
    
    required init(view: DocumentReaderView) {
        self.view = view
    }
    
    internal func presentDismiss() {
        isBack = true
    }
    
    internal func presentBackView() {
        isBack = true
        self.view.displayViewBack()
    }
    
    internal func presentViewInformation() {
        
        let model = DocumentReaderModel(title: kTitle, statusTextDetect: kTitle, result: Localization.withoutResults)
        view.displayViewInformation(model: model)
    }
}

extension DocumentReaderPresenter {
    
    func onMissingText() {
    }
    
    func onTextFeature(result: VisionText, mode: DocumentReaderMode) {
        
        var validationIsRepublic = false
        var validationIsId = false
        var validationIsBackDocument = false
        var isDocument = false
        var resultStr: String = ""
        
        for block in result.blocks {
            for line in block.lines {
                
                let lineText = line.text
                resultStr = resultStr + lineText + "\n"
                
                if (lineText.range(of:"REPUBLICA DE COLOMBIA") != nil) {
                    validationIsRepublic = true;
                }
                
                if (lineText.range(of: "CEDULA") != nil) {
                    validationIsId = true;
                }
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd-MM-yyyy"
                if dateFormatterGet.date(from: lineText) != nil {
                    validationIsBackDocument = true;
                }
                
                if (lineText.range(of: "INDICE DERECHO") != nil) {
                    validationIsBackDocument = true;
                }
                
                if ((mode == DocumentReaderMode.front && validationIsRepublic && validationIsId) || (mode == DocumentReaderMode.back && validationIsBackDocument)) {
                    isDocument = true
                }
            }
        }
        
        view.displayDocumentInformation(isSuccess: isDocument, result: resultStr)
    }
}

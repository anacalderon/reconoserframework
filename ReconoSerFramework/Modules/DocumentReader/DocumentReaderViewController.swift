//
//  DocumentReaderViewController.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import AVFoundation
import UIKit
import FirebaseMLVision

protocol DocumentReaderView: class {
    func displayViewInformation(model: DocumentReaderModel)
    func displayViewBack()
    func displayDocumentInformation(isSuccess:Bool, result:String)
}

public class DocumentReaderViewController: ReconoSerViewController {
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var img_mask: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    fileprivate var stillImageOutput: AVCapturePhotoOutput?
    fileprivate var input: AVCaptureDeviceInput!
    fileprivate var captureSession: AVCaptureSession!
    fileprivate var camera: AVCaptureDevice!
    fileprivate var currentModel: DocumentReaderModel?
    fileprivate var isDetectingText = false
    fileprivate var vision: Vision?
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer!
    
    var delegate: DocumentReaderDelegate?
    var mode:DocumentReaderMode?
    var presenter: DocumentReaderPresenter!
    
    private lazy var annotationOverlayView: UIView = {
        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOverlayView
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        configurePresenter()
        setUpAnnotationOverlayView()
        setMaskNormal()
        spinner.isHidden = true
        vision = Vision.vision()
        
        if (mode == DocumentReaderMode.front) {
            titleLabel.text = Localization.frontReader
        } else {
            titleLabel.text = Localization.backReader
        }
        
        subTitleLabel.text = Localization.documentInfo
        infoLabel.text = Localization.noDocumentDetected
    }
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        setupCamera()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if (captureSession == nil && camera == nil) {
            return
        }
        stopCaptureSession()
    }
    
    private func stopCaptureSession() {
        captureSession.stopRunning()
        for output in captureSession.outputs {
            captureSession.removeOutput(output)
            captureSession.removeOutput(stillImageOutput!)
        }
        
        for input in captureSession.inputs {
            captureSession.removeInput(input)
        }
        captureSession = nil
        camera = nil
    }
    
    private func setUpAnnotationOverlayView() {
        previewView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: previewView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: previewView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: previewView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: previewView.bottomAnchor),
            ])
    }
    
    private func removeDetectionAnnotations() {
        for annotationView in annotationOverlayView.subviews {
            annotationView.removeFromSuperview()
        }
    }
    
    private func setupCamera(){
        captureSession = AVCaptureSession()
        
        camera = AVCaptureDevice.default(
            .builtInWideAngleCamera,
            for: .video,
            position: .back)
        
        do {
            input = try AVCaptureDeviceInput(device: camera)
        } catch let error as NSError {
            print(error)
        }
        
        if captureSession.canAddInput(input) {
            captureSession.addInput(input)
        }
        
        let output = AVCaptureVideoDataOutput()
        stillImageOutput = AVCapturePhotoOutput()
        
        output.videoSettings =
            [(kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA]
        
        if captureSession.canAddOutput(output) {
            captureSession.addOutput(output)
            captureSession.addOutput(stillImageOutput!)
        }
        captureSession.commitConfiguration()
        
        let queue = DispatchQueue(label: "output.queue")
        output.setSampleBufferDelegate(self, queue: queue)
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.bounds
        previewLayer.videoGravity = .resizeAspectFill
        previewView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    // MARK: On-Device Detection
    private func getDetectText(image: VisionImage) {
        
        let textRecognizer = vision?.cloudTextRecognizer()
        textRecognizer?.process(image) { result, error in
            
            guard error == nil, let result = result else {
                print("Could not recognize any text")
                self.isDetectingText = false
                return
            }
            
            self.presenter.onTextFeature(result: result, mode: self.mode!)
        }
    }
    
    private func imageOrientation(
        deviceOrientation: UIDeviceOrientation,
        cameraPosition: AVCaptureDevice.Position
        ) -> VisionDetectorImageOrientation {
        switch deviceOrientation {
        case .portrait:
            return cameraPosition == .front ? .leftTop : .rightTop
        case .landscapeLeft:
            return cameraPosition == .front ? .bottomLeft : .topLeft
        case .portraitUpsideDown:
            return cameraPosition == .front ? .rightBottom : .leftBottom
        case .landscapeRight:
            return cameraPosition == .front ? .topRight : .bottomRight
        case .faceDown, .faceUp, .unknown:
            return .leftTop
        @unknown default:
            fatalError()
        }
    }
}

extension DocumentReaderViewController {
    
    private func configurePresenter() {
        if presenter == nil {
            presenter = DocumentReaderPresenter(view: self)
        }
        presenter.presentViewInformation()
    }
    
    private func setMaskNormal() {
        setMask(imageName: "bg_doc_camera")
    }
    
    private func setMask(imageName: String) {
        if UIDevice.current.screenFamily == .iPhonePlus {
            img_mask.image = UIImage(named: imageName + "_plus")
        } else if UIDevice.current.screenFamily == .iPhoneStandard  {
            img_mask.image = UIImage(named: imageName + "_normal")
        } else {
            img_mask.image = UIImage(named: imageName + "_x")
        }
    }
    
    public class func initFromStoryboard(guidAgreement:String, dataConv:String, mode:DocumentReaderMode, completionHandler: @escaping (DocumentReaderViewController?, ErrorEntransaccion?) -> ()) {
        
        let presenter = PublicDocumentReaderPresenter()
        presenter.validateAgreement(guidAgreement: guidAgreement, dataConv: dataConv, mode: mode, completionHandler: {(isSuccess, error) in
            
            if (isSuccess) {
                let storyboard = UIStoryboard(name: ReconoSerConstants.Name.DocumentReaderViewController.storyboard, bundle: Bundle(for: self))
                guard let viewController = storyboard.instantiateViewController(withIdentifier: ReconoSerConstants.Name.DocumentReaderViewController.idIdentifier) as? DocumentReaderViewController else {
                    fatalError("Unable to instantiate viewController \(ReconoSerConstants.Name.DocumentReaderViewController.idIdentifier) from storyboard \(ReconoSerConstants.Name.DocumentReaderViewController.storyboard)")
                }
                completionHandler(viewController, nil)
            } else {
                completionHandler(nil, error)
            }
        })
    }
    
    private func displayViewShowPhoto(imageData: Data) {
        if let image = UIImage(data: imageData) {
            
            stopCaptureSession()
            let square = image.size.width < image.size.height ? CGSize(width: image.size.width, height: image.size.width) : CGSize(width: image.size.height, height: image.size.height)
            let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
            imageView.contentMode = UIView.ContentMode.scaleAspectFill
            imageView.image = image
            imageView.layer.masksToBounds = true
            UIGraphicsBeginImageContext(imageView.bounds.size)
            imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            self.presenter.presentDismiss()
            self.dismiss(animated: true) {
                
                if (result == nil) {
                    let errorImage = ErrorEntransaccion(codigo: ReconoSerConstants.Error.ERROR_R106, descripcion: ReconoSerConstants.Error.ERROR_IMAGE_PROCESS)
                    
                    self.delegate?.onScanDocument(nil, imageData: nil, mode: self.mode!, result: nil, error: errorImage)
                    return
                }
                
                self.delegate?.onScanDocument(result!, imageData: imageData, mode: self.mode!, result: self.currentModel!.result, error: nil)
            }
        }
    }
}

extension DocumentReaderViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        if (self.isDetectingText) {
            return
        }
        self.isDetectingText = true
        
        let visionImage = VisionImage(buffer: sampleBuffer)
        let cameraPosition = AVCaptureDevice.Position.back
        let metadata = VisionImageMetadata()
        metadata.orientation = self.imageOrientation(
            deviceOrientation: UIDevice.current.orientation,
            cameraPosition: cameraPosition
        )
        visionImage.metadata = metadata
        
        if (mode == DocumentReaderMode.front) {
            let options = VisionFaceDetectorOptions()
            options.landmarkMode = .all
            options.isTrackingEnabled = true
            options.classificationMode = .all
            
            let faceDetector = vision?.faceDetector(options: options)
            faceDetector!.process(visionImage) { features, error in
                if let error = error {
                    print(error.localizedDescription)
                    self.isDetectingText = false
                    return
                }
                
                guard error == nil, let features = features, !features.isEmpty else {
                    print("On-Device face detector returned no results.")
                    self.subTitleLabel.text = Localization.withoutFace
                    self.isDetectingText = false
                    return
                }
                
                if (features.count > 1) {
                    self.subTitleLabel.text = Localization.moreFace
                    self.isDetectingText = false
                    return
                } else {
                    self.subTitleLabel.text = Localization.documentInfo
                }
                
                self.getDetectText(image: visionImage)
            }
        } else {
            self.getDetectText(image: visionImage)
        }
    }
}

extension DocumentReaderViewController: DocumentReaderView {
    internal func displayDocumentInformation(isSuccess: Bool, result: String) {
        
        if(isSuccess) {
            let settings = AVCapturePhotoSettings()
            let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
            let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                                 kCVPixelBufferWidthKey as String: 640,
                                 kCVPixelBufferHeightKey as String: 640]
            settings.previewPhotoFormat = previewFormat
            self.stillImageOutput!.capturePhoto(with: settings, delegate: self)
            self.currentModel?.result = result
            return
        } else {
            isDetectingText = false
        }
    }
    
    internal func displayViewBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    internal func displayViewInformation(model: DocumentReaderModel) {
        currentModel = model
        infoLabel.text = currentModel?.statusTextDetect
    }
}

extension DocumentReaderViewController: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        let imageData = photo.fileDataRepresentation()
        self.displayViewShowPhoto(imageData: imageData!)
    }
}

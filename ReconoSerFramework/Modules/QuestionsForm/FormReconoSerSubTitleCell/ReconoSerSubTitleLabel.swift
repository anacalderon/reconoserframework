//
//  ReconoSerSubTitleLabel.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import UIKit

class ReconoSerSubTitleLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

private extension ReconoSerSubTitleLabel {
    func configure() {
        textColor = UIColor.greyishBrown()
        font = UIFont.systemFont(ofSize: 15)
    }
}

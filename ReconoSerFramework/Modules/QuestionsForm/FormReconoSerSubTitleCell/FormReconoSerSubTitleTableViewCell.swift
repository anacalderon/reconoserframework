//
//  FormReconoSerSubTitleTableViewCell.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import UIKit

class FormReconoSerSubTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var subtitleLabel: ReconoSerSubTitleLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configurateCell(text: String) {
        self.subtitleLabel.text = text
        self.subtitleLabel.textColor = UIColor.greyishBrown()
        self.selectionStyle = .none
    }
}

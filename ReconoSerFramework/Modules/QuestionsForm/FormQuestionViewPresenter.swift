//
//  QuestionViewPresenter.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

protocol FormQuestionViewPresenterView: class {
    init(view: FormQuestionInfoView)
    func presentViewInformation()
}

internal class FormQuestionViewPresenter: FormQuestionViewPresenterView {
    unowned var view: FormQuestionInfoView
    var formQuestionModel: FormQuestionModel?
    
    required init(view:FormQuestionInfoView) {
        self.view = view
    }
    
    func presentViewInformation() {
        view.displayViewInformation(model: formQuestionModel)
    }
}

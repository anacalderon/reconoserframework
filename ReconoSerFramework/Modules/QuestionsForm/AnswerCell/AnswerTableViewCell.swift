//
//  AnswerTableViewCell.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import UIKit

class AnswerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 4
        borderView.layer.borderColor = UIColor.brownGrey().cgColor
        self.backgroundColor = UIColor.clear
        label.textColor = UIColor.greyishBrown()
        label.font = UIFont.systemFont(ofSize: 15)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

//
//  FormQuestionModel.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

struct FormQuestionModel {
    var title: String
    var idQuestionnaire:String
    var questionnaireRegistration:Int
    var formInputElements: [Pregunta]
}

//
//  QuestionViewController.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import UIKit

public protocol FormQuestionViewControllerDelegate: class {
    func dataAnswerSend(_ idQuestionnaire:String, questionnaireRegistration:Int, answers:[RespuestasIn])
}

protocol FormQuestionInfoView: class {
    func displayViewInformation(model: FormQuestionModel?)
}

public class FormQuestionViewController: ReconoSerViewController {
    
    @IBOutlet weak var tableAnswer: UITableView!
    @IBOutlet weak var img_mask: UIImageView!
    
    fileprivate var formQuestionModel: FormQuestionModel?
    fileprivate var currentPregunta: Pregunta?
    fileprivate var currentIndexPregunta: Int = 0
    
    var presenter: FormQuestionViewPresenter!
    var delegate: FormQuestionViewControllerDelegate?
    var answers: [RespuestasIn]?
    
    init() {
        super.init(nibName: ReconoSerConstants.kFormQuestionViewController, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        title = formQuestionModel?.title ?? "Preguntas de seguridad"
        configurePresenter()
        configureUI()
        answers = []
    }
    
    func setPresenter(presenter: FormQuestionViewPresenter) {
        if self.presenter != nil {
            return
        }
        self.presenter = presenter
    }
    
    private func configurePresenter() {
        if presenter == nil {
            presenter = FormQuestionViewPresenter(view: self)
        }
        presenter.presentViewInformation()
    }
    
    private func configureUI() {
        
        tableAnswer.register(UINib(nibName: ReconoSerConstants.kAnswerViewCell,
                                   bundle: nil),
                             forCellReuseIdentifier: ReconoSerConstants.kAnswerViewCell)
        tableAnswer.register(UINib(nibName: ReconoSerConstants.kFormReconoSerSubTitleTableViewCell,
                                   bundle: nil),
                             forCellReuseIdentifier: ReconoSerConstants.kFormReconoSerSubTitleTableViewCell)
        tableAnswer.delegate = self
        tableAnswer.dataSource = self
        
        let str:Int = (formQuestionModel?.formInputElements.count ?? 0)
        self.title = "PREGUNTA \(currentIndexPregunta + 1) DE \(str)"
        
        setMaskNormal()
    }
    
    private func setMaskNormal() {
        setMask(imageName: "bg_white")
    }
    
    private func setMask(imageName: String) {
        if UIDevice.current.screenFamily == .iPhonePlus {
            img_mask.image = UIImage(named: imageName + "_plus")
        } else if UIDevice.current.screenFamily == .iPhoneStandard  {
            img_mask.image = UIImage(named: imageName + "_normal")
        } else {
            img_mask.image = UIImage(named: imageName + "_x")
        }
    }
}

extension FormQuestionViewController: FormQuestionInfoView {
    func displayViewInformation(model: FormQuestionModel?) {
        self.formQuestionModel = model
        _showQuestion()
    }
    
    func _showQuestion() {
        currentPregunta = formQuestionModel?.formInputElements[currentIndexPregunta]
    }
}

extension FormQuestionViewController: UITableViewDataSource {
    
    private func configurateCellsInputsSection(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ReconoSerConstants.kFormReconoSerSubTitleTableViewCell,
                                                     for: indexPath) as? FormReconoSerSubTitleTableViewCell
            cell?.configurateCell(text: currentPregunta?.textoPregunta ?? "")
            return cell!
        }
        
        let form = currentPregunta?.opcionesRespuestas[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ReconoSerConstants.kAnswerViewCell,
                                                 for: indexPath) as? AnswerTableViewCell
        cell?.label.text = form?.textoRespuesta
        return cell!
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configurateCellsInputsSection(indexPath: indexPath, tableView: tableView)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return 1
        }
        return currentPregunta?.opcionesRespuestas.count ?? 0
    }
}

extension FormQuestionViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (currentPregunta != nil) {
            let responseData = RespuestasIn(idPregunta: currentPregunta!.idPregunta, idRespuesta: currentPregunta!.opcionesRespuestas[indexPath.row].idRespuesta)
            answers!.append(responseData)
        }
        
        if (currentIndexPregunta >= (formQuestionModel?.formInputElements.count ?? 0) - 1 ) {
            print("Termino")
            self.dismiss(animated: true) {
                self.delegate?.dataAnswerSend(self.formQuestionModel!.idQuestionnaire, questionnaireRegistration: self.formQuestionModel!.questionnaireRegistration, answers: self.answers!)
            }
            return
        }
        
        currentIndexPregunta = currentIndexPregunta + 1
        let str:Int = (formQuestionModel?.formInputElements.count ?? 0)
        self.title = "PREGUNTA \(currentIndexPregunta + 1) DE \(str)"
        
        _showQuestion()
        tableView.reloadData()
    }
}

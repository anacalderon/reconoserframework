//
//  UIColor+ReconoSer.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func greyishBrown() -> UIColor {
        return UIColor(red: 63/255.0, green: 63/255.0, blue: 63/255.0, alpha: 1.0)
    }
    
    class func brownGrey() -> UIColor {
        return UIColor(red: 133/255.0, green: 133/255.0, blue: 133/255.0, alpha: 1.0)
    }
}

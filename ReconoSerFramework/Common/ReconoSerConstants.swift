//
//  ReconoSerConstants.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

struct ReconoSerConstants {
    struct Name {
        
        struct BiometricReaderViewController {
            static let storyboard = "BiometricStoryboard"
            static let idIdentifier = "CameraUserValidationViewController"
        }
        
        struct DocumentReaderViewController {
            static let storyboard = "DocumentStoryboard"
            static let idIdentifier = "DocumentReaderViewController"
            static let nameService = "Biometría"
            static let typeService = "Documento"
            static let nameSubserviceFront = "Anverso"
            static let nameSubserviceBack = "Reverso"
        }
        
        struct BarcodeReaderViewController {
            static let storyboard = "DocumentStoryboard"
            static let idIdentifier = "BarcodeReaderViewController"
            static let nameService = "Biometría"
            static let typeService = "Documento"
            static let nameSubserviceBarcode = "Barcode"
        }
        
        struct OTP {
            static let nameService = "OTP"
            static let typeServiceSMS = "SMS"
            static let typeServiceEmail = "Email"
            static let SubTypeService = ""
        }
        
        struct Biometrics {
            static let nameService = "Biometría"
            static let typeServiceFacial = "Facial"
            static let typeServiceDocument = "Documento"
            static let nameSubserviceFront = "Anverso"
            static let nameSubserviceBack = "Reverso"
            static let nameSubserviceBarcode = "Barcode"
            static let nameSubserviceFacialFront = "Frontal"
        }
    }
    
    struct Error {
        
        static let ERROR_HOST = "No se pudo hacer conexión con el servidor"
        static let ERROR_R100 = "R_100"
        static let ERROR_CONV = "No esta activo el convenio"
        static let ERROR_R101 = "R_101"
        static let ERROR_SERVICE_API = "El convenio no tiene habilitado este servicio"
        static let ERROR_R102 = "R_102"
        static let ERROR_SERVER = "An error has occurred."
        static let ERROR_R103 = "R_103"
        static let ERROR_IMAGE_LOAD = "Error al cargar la imagen"
        static let ERROR_R104 = "R_104"
        static let ERROR_IMAGE_SAVE = "Error al guardar la imagen"
        static let ERROR_R105 = "R_105"
        static let ERROR_IMAGE_PROCESS = "No se pudo procesar la imagen"
        static let ERROR_R106 = "R_106"
    }
    
    static let baseUrl = "https://tzr3anh3h6.execute-api.us-east-2.amazonaws.com/dev/"
    static let olimpiaBaseUrl = "https://demorcs.olimpiait.com:6316/"
    
    static let kFormQuestionViewController = "FormQuestionViewController"
    static let kReconoSerTextFieldViewCell = "ReconoSerTextFieldViewCell"
    static let kAnswerViewCell = "AnswerTableViewCell"
    static let kFormReconoSerSubTitleTableViewCell = "FormReconoSerSubTitleTableViewCell"
}

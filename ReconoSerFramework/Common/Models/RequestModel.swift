//
//  RequestModel.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import AVFoundation

public struct CiudadanoIn {
    public var guidConv: String
    public var guidCiu: String
    public var tipoDoc: String
    public var numDoc: String
    public var email: String
    public var codPais: String
    public var celular: String
    public var datosAdicionales: String
    public init(guidConv:String, guidCiu:String, tipoDoc:String, numDoc:String, email:String, codPais:String, celular:String, datosAdicionales:String) {
        self.guidConv = guidConv
        self.guidCiu = guidCiu
        self.tipoDoc = tipoDoc
        self.numDoc = numDoc
        self.email = email
        self.codPais = codPais
        self.celular = celular
        self.datosAdicionales = datosAdicionales
    }
}

public struct DatosOTP {
    public var tipoOTP: String
    public var mensaje:String
    public init(tipoOTP: String, mensaje: String) {
        self.tipoOTP = tipoOTP
        self.mensaje = mensaje
    }
}

public struct ValidarBiometriaIn {
    public var guiCiudadano: String
    public var idServicio: String
    public var subtipo: String
    public var biometria: String
    public var formato: String
    public var datosAdicionales: String
    public var usuario: String
    public init(guiCiudadano:String, idServicio:String, subtipo:String, biometria:String, formato:String, datosAdicionales:String, usuario:String) {
        self.guiCiudadano = guiCiudadano
        self.idServicio = idServicio
        self.subtipo = subtipo
        self.biometria = biometria
        self.formato = formato
        self.datosAdicionales = datosAdicionales
        self.usuario = usuario
    }
}

public struct GuardarLogErrorIn {
    public
    var guidConv: String
    public var texto:String
    public var componente:String
    public var usuario:String
    public init(guidConv:String, texto:String, componente:String, usuario:String) {
        self.guidConv = guidConv
        self.texto = texto
        self.componente = componente
        self.usuario = usuario
    }
}

public struct RespuestasIn {
    public var idPregunta: String
    public var idRespuesta:String
    public init(idPregunta:String, idRespuesta:String) {
        self.idPregunta = idPregunta
        self.idRespuesta = idRespuesta
    }
}

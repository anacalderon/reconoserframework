//
//  ConsultarConvenio.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import AVFoundation

public struct Ciudadano {
    public var guidCiu: String
    public var guidConv: String
    public var tipoDoc: String
    public var numDoc: String
    public var email: String
    public var codPais: String
    public var celular: String
    public var datosAdi: String
}

public struct ServiciosConv {
    public var idServicio: Int
    public var nombreServicio: String
    public var tipoServicio: String
    public var subtipo: String
    public var nivelCaptura: Int
    public var continuarProceso: Bool
    public var reintentos: Int
    public var esperaReintentos: Int
}

public struct ErrorEntransaccion: CustomStringConvertible {
    public var description:String {
        return "(codigo:\(codigo), descripcion:\(descripcion))"
    }
    public var codigo: String
    public var descripcion: String
}

public struct Pregunta {
    public var idPregunta: String
    public var textoPregunta: String
    public var opcionesRespuestas: [OpcionRespuesta]
}

public struct OpcionRespuesta {
    public var idRespuesta: String
    public var textoRespuesta: String
}

public struct Status {
    public var servicio: String
    public var terminado: Bool
}

public struct ConsultarConvenio: CustomStringConvertible {
    public var description: String {
        return "(estadoActivo:\(estadoActivo), serviciosConv:\(serviciosConv), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var estadoActivo: Bool
    public var serviciosConv:[ServiciosConv]
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct GuardarCiudadano: CustomStringConvertible {
    public var description: String {
        return "(guidCiu:\(guidCiu), primerNombre:\(primerNombre), segundoNombre:\(segundoNombre), primerApellido:\(primerApellido), segundoApellido:\(segundoApellido), vivo:\(vivo), estadoDoc:\(estadoDoc), fechaExp:\(fechaExp), fechaNac:\(fechaNac), localizacion:\(localizacion), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var guidCiu: String
    public var primerNombre: String
    public var segundoNombre: String
    public var primerApellido: String
    public var segundoApellido: String
    public var vivo: Bool
    public var estadoDoc: String
    public var fechaExp: String
    public var fechaNac: String
    public var localizacion: String
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct GuardarBiometria: CustomStringConvertible {
    public var description: String {
        return "(guidBio:\(guidBio), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var guidBio: String
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct EnviarOTP: CustomStringConvertible {
    public var description: String {
        return "(guidOTP:\(guidOTP), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var guidOTP: String
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct ValidarOTP: CustomStringConvertible {
    public var description: String {
        return "(esValida:\(esValida), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var esValida: Bool
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct SolicitarPreguntaDemografica: CustomStringConvertible {
    public var description: String {
        return "(idCuestionario:\(idCuestionario), registerQuestionnaire:\(registroCuestionario), Preguntas:\(Preguntas), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var idCuestionario: String
    public var registroCuestionario: Int
    public var Preguntas: [Pregunta]
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct ValidarRespuestaDemografica: CustomStringConvertible {
    public var description: String {
        return "(score:\(score), esValido:\(esValido), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var score: String
    public var esValido: Bool
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct ConsultarCiudadano: CustomStringConvertible {
    public var description: String {
        return "(ciudadano:\(ciudadano), status:\(status), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var ciudadano: Ciudadano
    public var status: [Status]
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct ValidarBiometria: CustomStringConvertible {
    public var description: String {
        return "(score:\(score), esValido:\(esValido), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var esValido: Bool
    public var score: String
    public var respuestaTransaccion: RespuestaTransaccion
}

public struct RespuestaTransaccion: CustomStringConvertible {
    public var description: String {
        return "esExitosa:\(esExitosa)), respuestaTransaccion:\(String(describing: errorEntransaccion)))"
    }
    public var esExitosa: Bool
    public var errorEntransaccion: ErrorEntransaccion?
}

public struct GuardarLogError: CustomStringConvertible {
    public var description: String {
        return "esExitosa:\(esExitosa)), respuestaTransaccion:\(String(describing: errorEntransaccion)))"
    }
    public var esExitosa: Bool
    public var errorEntransaccion: ErrorEntransaccion?
}

public struct CompararRostro: CustomStringConvertible {
    public var description: String {
        return "Similitud:\(Similitud))"
    }
    public var Similitud: Double
}

public struct GuardarDocumento: CustomStringConvertible {
    public var description: String {
        return "(numDoc:\(numDoc), primerApellido:\(primerApellido), segundoApellido:\(segundoApellido), primerNombre:\(primerNombre), segundoNombre:\(segundoNombre), sexo:\(sexo), fechaDeNacimiento:\(fechaDeNacimiento), rh:\(rh), respuestaTransaccion:\(respuestaTransaccion))"
    }
    public var numDoc: String
    public var primerApellido: String
    public var segundoApellido: String
    public var primerNombre: String
    public var segundoNombre: String
    public var sexo: String
    public var fechaDeNacimiento: String
    public var rh: String
    public var respuestaTransaccion: RespuestaTransaccion
}

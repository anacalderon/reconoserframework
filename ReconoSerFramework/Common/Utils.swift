//
//  Utils.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

extension String {
    func localized(withComment comment: String = "") -> String {
        return NSLocalizedString(self, bundle: Bundle.module, comment: comment)
    }
}

extension Bundle {
    private static let bundleID = "co.ReconoSerFramework"
    static var module: Bundle {
        guard let path = Bundle(identifier: bundleID)?.resourcePath else { return .main }
        return Bundle(path: path.appending("/ReconoSer.bundle")) ?? .main
    }
}

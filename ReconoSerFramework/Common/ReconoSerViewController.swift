//
//  ReconoSerViewController.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import UIKit

public class ReconoSerViewController: UIViewController {
    override public func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        
        if  self.navigationController?.navigationBar.subviews.isEmpty == true {
            return
        }
        
        if (self.navigationController?.navigationBar.subviews) == nil {
            return
        }
        
        var count = 0
        for item in(self.navigationController?.navigationBar.subviews)! {
            for sub in item.subviews {
                if sub is UILabel {
                    if count == 1 {
                        break
                    }
                    
                    let titleLab: UILabel = (sub as? UILabel)!
                    titleLab.numberOfLines = 0
                    
                    titleLab.text = self.title
                    titleLab.lineBreakMode = .byWordWrapping
                    count += 1
                }
            }
        }
        
        self.navigationController?.view.layoutSubviews()
        self.navigationController?.view.layoutIfNeeded()
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

private extension ReconoSerViewController {
    private func configureUI() {
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.black,
             NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)]
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.largeTitleTextAttributes =  [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 28)]
        }
    }
}

//
//  Localization.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

internal enum Localization {
    
    // BIOMETRIC READER
    public static let noFaceDetected = "No hay un rostro detectado"
    public static let blinksSoftly = "Pestañea suavemente"
    public static let smile = "Sonríe"
    public static let keepTurning = "Sigue girando"
    public static let turnHeadLeft = "Gira la cabeza a tu izquierda"
    public static let turnHeadRight = "Gira la cabeza a tu derecha"
    public static let centerFace = "Gira la cabeza al centro"
    public static let faceError = "\n\nError: Se detecta más de un rostro"
    public static let isOK = "¡Correcto!"
    
    // DOCUMENT READER
    public static let noDocumentDetected = "Escanea el documento con buena luz"
    public static let withoutResults = "Sin resultados"
    public static let frontReader = "Lector Frontal"
    public static let backReader = "Lector Trasero"
    public static let documentInfo = "Debe verse claramente tu foto y datos personales"
    public static let withoutFace = "No se ha detectado un rostro"
    public static let moreFace = "Se ha detectado más de un rostro"
}

//
//  CameraUserValidationUI.swift
//  ReconoSerFramework
//
//  Created by Olimpia IT on 8/08/19.
//  Copyright © 2019 Olimpia IT. All rights reserved.
//

import Foundation

public enum DocumentReaderMode {
    case front
    case back
}

// - Delegates -

public protocol BiometricReaderDelegate: class {
    
    func onScanFace(_ image:UIImage?, imageData:Data?, result:CompararRostro?, error:ErrorEntransaccion?)
}

public protocol DocumentReaderDelegate: class {
    
    func onScanDocument(_ image:UIImage?, imageData:Data?, mode:DocumentReaderMode, result:String?, error:ErrorEntransaccion?)
}

public protocol BarcodeReaderDelegate: class {
    
    func onScanBarcode(result: String)
}

// - Readers -

@objc public class  BiometricReader: NSObject {
    
    public func getBiometricReaderViewController(guidAgreement:String, dataConv:String, delegate: BiometricReaderDelegate, imageToCompare:UIImage?, completionHandler: @escaping (BiometricReaderViewController?, ErrorEntransaccion?) -> ()) {
        
        BiometricReaderViewController.initFromStoryboard(guidAgreement: guidAgreement, dataConv: dataConv, completionHandler: {(biometricReaderViewController, error) in
            
            if (error != nil) {
                completionHandler(nil, error)
                return
            }
            biometricReaderViewController!.delegate = delegate
            biometricReaderViewController!.imageToCompare = imageToCompare
            completionHandler(biometricReaderViewController, nil)
        })
    }
    
    @objc public override init() {
        super.init()
    }
}

@objc public class  DocumentReader: NSObject {
    
    // MARK: - Public -
    
    public func getDocumentReaderViewController(guidAgreement:String, dataConv:String, delegate:DocumentReaderDelegate, mode:DocumentReaderMode, completionHandler: @escaping (DocumentReaderViewController?, ErrorEntransaccion?) -> ()) {
        
        DocumentReaderViewController.initFromStoryboard(guidAgreement: guidAgreement, dataConv: dataConv, mode: mode, completionHandler: {(documentReaderViewController, error) in
            
            if (error != nil) {
                completionHandler(nil, error)
                return
            }
            documentReaderViewController!.delegate = delegate
            documentReaderViewController!.mode = mode
            completionHandler(documentReaderViewController, nil)
        })
    }
    
    @objc public override init() {
        super.init()
    }
}

@objc public class  BarcodeReader: NSObject {
    
    // MARK: - Public -
    
    public func getBarcodeReaderViewController(guidAgreement:String, dataConv:String, delegate: BarcodeReaderDelegate, completionHandler: @escaping (BarcodeReaderViewController?, ErrorEntransaccion?) -> ()) {
        
        BarcodeReaderViewController.initFromStoryboard(guidAgreement: guidAgreement, dataConv: dataConv, completionHandler: {(barcodeReaderViewController, error) in
            
            if (error != nil) {
                completionHandler(nil, error)
                return
            }
            barcodeReaderViewController!.delegate = delegate
            completionHandler(barcodeReaderViewController, nil)
        })
    }
    
    @objc public override init() {
        super.init()
    }
}

// - View Controllers -

@objc public class  FormQuestions: NSObject {
    
    public func getFormQuestionViewController(guidCitizen: String, delegate: FormQuestionViewControllerDelegate, completionHandler: @escaping (FormQuestionViewController?, ErrorEntransaccion?) -> ()) {
        
        ServiciosOlimpia.sharedInstance.solicitarPreguntasDemograficas(guidCiu: guidCitizen) { (solicitarpregunta, error) in
            
            let formQuestion = FormQuestionViewController()
            let displayFormView = FormQuestionViewPresenter(view: formQuestion)
            displayFormView.formQuestionModel = FormQuestionModel(title: "Preguntas de seguridad", idQuestionnaire: solicitarpregunta!.idCuestionario, questionnaireRegistration: solicitarpregunta!.registroCuestionario, formInputElements: solicitarpregunta!.Preguntas)
            
            formQuestion.setPresenter(presenter: displayFormView)
            formQuestion.delegate = delegate
            
            completionHandler(formQuestion, nil)
        }
    }
    
    @objc public override init() {
        super.init()
    }
}

Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "ReconoSer"
s.summary = "Library Olimpia IT."
s.requires_arc = true

# 2
s.version = "0.0.8"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name
s.author = { "Olimpia IT" => "olimpia_it@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://anacalderon@bitbucket.org/anacalderon/reconoserframework"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://anacalderon@bitbucket.org/anacalderon/reconoserframework.git",
:tag => "#{s.version}" }

# 7

s.static_framework = true

s.dependency 'Firebase'
s.dependency 'Firebase/Core'
s.dependency 'Firebase/MLVision'
s.dependency 'Firebase/MLVisionFaceModel'

# 8
s.source_files = "reconoserframework/**/*.{swift}"

# 9
s.resources = "reconoserframework/**/*.{png,jpeg,jpg,storyboard,xib,xcassets, strings}"

# 10
s.swift_version = "4.2"

end
